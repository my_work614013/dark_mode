import Switch from 'react-switch'
import { useContext } from 'react'
import { ThemeContext } from '../App'

const Title = ()=>{

    const {theme, setTheme} = useContext(ThemeContext)

    const toggleSwitch = (checked) =>{
        setTheme(
            // ?=จริงจะเเสดง dark :=เท็จจะเเสดง light
            //ternary เช็ค if-else แบบลดรูป --> condition ? value_if_true : value_if_false
            theme === "light" ? "dark" : "light"
        )
    }

    return(
        <header className={theme==="dark" ? "dark" : "light"}>
            <span>Mode [{theme}]</span>
            <Switch
                onChange = {toggleSwitch}
                checked = {theme==="dark"}
                uncheckedIcon={false}
                checkedIcon={false}
                onColor={'#ffa500'}
            />
        </header>
    )
}

export default Title